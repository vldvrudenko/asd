import React from 'react'
import styles from './Card.module.scss'

const Card =()=>{
    return(
        <div className={styles.Card}>
                <div className={styles.Card__header}>
                    <div className={styles.Card__userLogo}></div>
                    <div className={styles.Card__userDate}>
                        <div className={styles.Card__userName}>Volodia</div>
                        <div className={styles.Card__userPosition}>Ukraine </div>
                    </div>
                    <div className={styles.Card__burger}>
                           ...
                        </div>
                </div>
                <div  className={styles.Card__content}></div>
                <div className={styles.Card__communication}>

                <span class="material-icons">
                    favorite
                </span>
                <span class="material-icons">
                    send
                </span>
                <span class="material-icons">
                    try
                </span>

        </div>
        <div className={styles.Card__footer}>
            <input type="text" placeholder="Добавьте комментарий" class={styles.Card__inputComment} />
            <div className={styles.Card_button}>Поделиться</div>
        </div>
        
    </div>
    
    )
}

export default Card