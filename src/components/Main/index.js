import React, { useState } from 'react'
import styles from './Main.module.scss'
import Card from '../Card'
import Slider from '../Slider'

const Main =()=>{
    const[cards] = useState([1,2,3])
    
    return(
        <div className={styles.Main}>
            <div className={styles.Main__cardWrapper}>
               <Slider />  
              
                <div className={styles.Main__cardWrapper}>
                     
                      {cards.map(()=>{
                          return( 
                            <Card />
                          )
                      })}
                </div>
            </div>
           <div className={styles.Main__recomendation}>           
                 <div className={styles.Main__header}>

                    <div className={styles.Main__userLogo}></div>
                    <div className={styles.Main__userDate}>
                        <div className={styles.Main__userName}>Lans</div>
                        <div className={styles.Main__userPosition}>Ukraine </div>
                    </div>
                        <div className={styles.Main__reloc}>
                           Переключится
                        </div>
                        
                </div>

                <div className={styles.Main__history}>
                    <div className={styles.Main__overflow}>
                        <div className={styles.Main__overflowPosition}>
                            <h5 className={styles.Main__historyText}>Истории</h5>
                            <h5 className={styles.Main__historyCheck}>Просмотреть все</h5>
                </div>
                <div className={styles.Main__scroll}>
                            <div className={styles.Main__friends}>
                      <div className={styles.Main__logoFriends}></div>
                      <div className={styles.Main__nick}>Antonio</div>
                        <button className={styles.Main__status}>Cтежити</button>

                        </div>
                        <div className={styles.Main__friends}>
                      <div className={styles.Main__logoFriends}></div>
                      <div className={styles.Main__nick}>Antonio</div>
                        <button className={styles.Main__status}>Cтежити</button>
                </div>
                <div className={styles.Main__friends}>
                      <div className={styles.Main__logoFriends}></div>
                      <div className={styles.Main__nick}>Antonio</div>
                        <button className={styles.Main__status}>Cтежити</button>
                </div>
                <div className={styles.Main__friends}>
                      <div className={styles.Main__logoFriends}></div>
                      <div className={styles.Main__nick}>Antonio</div>
                        <button className={styles.Main__status}>Cтежити</button>
                </div>
                    </div>
                    </div>
                </div>
                <div className={styles.Main__fullRecomandation}>
                <div className={styles.Main__positionText}>
                    <div className={styles.Main__information}>Рекомендация для вас</div>   
                    <div className={styles.Main__colorPosition}>Переглянути всіх</div>                 
                </div>
 
                <div className={styles.Main__friends}>
                      <div className={styles.Main__logoFriends}></div>
                      <div className={styles.Main__nick}>Antonio</div>
                        <button className={styles.Main__status}>Cтежити</button>


                </div>
                <div className={styles.Main__friends}>
                      <div className={styles.Main__logoFriends}></div>
                      <div className={styles.Main__nick}>Antonio</div>
                        <button className={styles.Main__status}>Cтежити</button>


                </div>
                <div className={styles.Main__friends}>
                      <div className={styles.Main__logoFriends}></div>
                      <div className={styles.Main__nick}>Antonio</div>
                        <button className={styles.Main__status}>Cтежити</button>


                </div>
                           
                </div>
                <div className={styles.Main__footer}>
                    <div className={styles.Main__block}>
                        <div>Информация Помощь Пресса API </div>
                        <div>Вакансии Конфиденциальность Условия</div>
                        <div> Места Популярные аккаунты Хэштеги Язык</div>
                         <div>© INSTAGRAM ОТ FACEBOOK, 2021</div> 
                    </div>
                </div>
            </div> 
        </div>
    )
}

export default Main