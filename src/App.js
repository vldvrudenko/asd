import { Route , Switch } from 'react-router-dom'
import Header from './components/Header'
import Main from './components/Main'
import Profile from './components/Profile';

function App() {
  return (
    <div className="App">
     <Header />      
    <Switch>
      <Route exact path="/" component={Main} />
      <Route exact path="/profile" component={Profile} />
    </Switch>
    </div>
  );
}

export default App;
